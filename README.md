
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Features

INTRODUCTION
------------

One Click Accessibility lets your site visitors adjust text, contrast, background, and other readability features from an Accessibility Tools frame added to your site. 

CONFIGURATION
-------------

To configure Position One Click Accessibility, go to:
pathL/admin/one-click-accessibility

To add Block search for: One Click Accessibility

FEATURES
--------
Accessibility Tools have a:
 * Increase/Decrease Text size
 * Grayscale
 * High Contrast
 * Negative Contrast
 * Light Background
 * Links Underline
 * Readable Font