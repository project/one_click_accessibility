<?php

namespace Drupal\one_click_accessibility\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class OneClickAccessibilityForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */

  public function getFormId() {
    return 'one_click_accessibility_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('one_click_accessibility.settings');
    
    //position
    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => [
        'right' => $this->t('Right'),
        'left' => $this->t('Left'),
      ],
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $config->get('one_click_accessibility.position'),
    ];

    $form['actions']['#type']= 'actions';
    $form['actions']['submit']= [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    
    $form_state->setCached(FALSE);

    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
   $config = $this->config('one_click_accessibility.settings');
    
    $config->set(
      'one_click_accessibility.position',
      $form_state->getValue('position'));

      $config->save();

     return parent::submitForm($form, $form_state);
    }

   /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'one_click_accessibility.settings',
    ];
  }
}