<?php
/**
 * @file
 * Contains \Drupal\one_click_accessibility\Plugin\Block\OneCilickAccessibilityBlock.
 */

namespace Drupal\one_click_accessibility\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use \Drupal\Core\Link;
use \Drupal\Core\Url;

/**
 * Provides a one click accessibility Block.
 *
 * @Block(
 *   id = "one_click_accessibility_block",
 *   admin_label = @Translation("One Click Accessibility block"),
 * )
 */
class OneCilickAccessibilityBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $settings = \Drupal::config('one_click_accessibility.settings')->get();
    return [
      '#theme' => 'one_click_accessibility_theme',
       '#settings' => $settings, # Assigns a value to the {{ test }} variable.
       '#attached' => [
        'library' => [
          'one_click_accessibility/accessibility',
        ]
      ],
    ];
  }

}
